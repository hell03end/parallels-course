#ifdef _WIN32
#include <limits.h>
#include <intrin.h>
typedef unsigned __int32  uint32_t;
#else
#include <stdint.h>
#endif

// uint32_t cpu_info[4] = { eax, ebx, ecd, edx };
void run_cpuid(uint32_t* cpu_info, unsigned int info_type, unsigned int cpuid_function) {
#ifdef _WIN32
    __cpuid((int*) cpu_info, (int) info_type, (int) cpuid_function);
#else
    asm volatile("cpuid"
        : "=a"(cpu_info[0]), "=b"(cpu_info[1]), "=c"(cpu_info[2]), "=d"(cpu_info[3])
        : "0"(info_type), "2"(cpuid_function)
    );
    // asm volatile {
    //     mov    esi, cpu_info
    //     mov    eax, info_type
    //     xor    ecx, ecx
    //     cpuid
    //     mov    dword ptr [esi +  0], eax
    //     mov    dword ptr [esi +  4], ebx
    //     mov    dword ptr [esi +  8], ecx
    //     mov    dword ptr [esi + 12], edx
    // }
#endif
}
