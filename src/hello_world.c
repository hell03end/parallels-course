#include <stdio.h>

int main(int argc, char const *argv[]) {
    // constant literal
    printf("%s\n", "init");

    // char array
    char* greeting = "Hello, world!"; // 13
    printf("%s\n", greeting);

    // char array throw cycle
    greeting = "end\n"; // char array reuse
    short len = 4; // (unsafe) it's possible to get data out of array bounds
    for (short i = 0; i < len; ++i) {
        printf("%c", *(greeting + i)); // greeting[i]
    }

    return 0;
}
