#include <stdio.h>
#include <time.h>
#include "cpuid.h"

const unsigned n_iterations = 100000000;
unsigned long counter = 0;

unsigned long iteration();

int main(int argc, char const *argv[]) {
    uint32_t cpu_info[4];

    clock_t begin = clock();
    for (unsigned i = 0; i < n_iterations; ++i) {
        run_cpuid(cpu_info, 0, 0);
        iteration();
    }
    clock_t end = clock();

    double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;

    printf("Time spent:\t%f\n", time_spent);
    printf("Iter/sec:\t%f\n", (double) n_iterations / time_spent);
    printf("CPU info:\t%d %d %d %d\n", (int) cpu_info[0], (int) cpu_info[1],
                                       (int) cpu_info[2], (int) cpu_info[3]);

    return 0;
}

unsigned long iteration() {
    return ++counter;
}
