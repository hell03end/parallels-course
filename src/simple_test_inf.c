#include <stdio.h>
#include "cpuid.h"

unsigned long counter = 0;

unsigned long iteration();

int main(int argc, char const *argv[]) {
    uint32_t cpu_info[4];

    for ( ; ; ) {
        run_cpuid(cpu_info, 0, 0);
        iteration();
    }

    return 0;
}

unsigned long iteration() {
    return ++counter;
}
