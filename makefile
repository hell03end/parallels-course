# ----- Configs -----
# specify compiler
CC=gcc
# specify compiler options (-Ofast)
OPT=-O1 -g3 -Wextra
# filenames to compile
COMPFILES = hello_world simple_test simple_test_inf

# output dir
OUT=bin/
# input files dir
SRC=src/

# ----- Commands -----
all: init build

init:
	if [ ! -e $(OUT) ]; then mkdir $(OUT); fi

build: $(COMPFILES)

clean:
	rm -rf $(OUT)

rebuild: clean init build

# ----- Internal -----
# compile input files to output dir
%: $(SRC)%.c
	$(CC) $(OPT) $^ -o $(OUT)$@
