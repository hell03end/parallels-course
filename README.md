Parallels course in MIEM HSE
============================

Task 1
------
1. Create `linux-vm` from `QUEMU` system
2. Create hello-world-application and compile it (`gcc`, `llvm`, etc.)
3. Copy result on VM and check it works in the same way
4. Profile application on both systems and compare results

Task 2
------
### Variant-1
1. Create 2 tests:
    a. 100_000_000 iterations-cycle with Intel `CPUID` instruction.
        print: time of execution, number of iterations per second.
    b. infinite cycle with Intel `CPUID` instruction for later profiling.

    Results should be received on:

    * host-system
    * `qemu` VM
    * `qemu-kvm` VM

2. Profile application with infinite cycle on each machine with `perf`.
    Compare and study results.

### Variant-2
1. Create simple test with infinite cycle, which increments global variable (in memory).
2. With Intel `CPUID` instruction with special argument report to VM about start of the test.
3. Study disassembled code (its instructions).
4. Modify `QEMU` code and receive disassembled listing of test after `QEMU` binary translation:
    * on `QEMU` in linux-user-mode
    * on `QEMU` in system-mode (inside VM)

    Compare results.

Prepare
-------
```bash
#!/bin/bash
# SHOULD BE RUN UNDER ROOT USER (e.g. `sudo -s`)
# WARNING: some packages may not exists - it's probably ok

# ----- For ubuntu -----
apt update && apt upgrade

# Packages for QEMU and KVM
apt install qemu-system
apt install qemu-utils
apt install libvirt-bin
apt install virt-manager
apt install virt-viewer
apt install python-spice-client-gtk # optional
apt install gir1.2-spice-client-gtk-3.0 # optional
# kvm
apt install linux-tools-virtual
apt install qemu-kvm
apt install virtinst
apt install bridge-utils # optional

# Developer tools
apt install git
apt install g++
apt install libglib2.0-dev
apt install libfdt-dev
apt install libpixman-1-dev
apt install zlib1g-dev
apt install gcc
apt install make

# profiling installation
apt install linux-tools-common
apt install linux-tools-generic
apt install perf
apt install linux-tools-4.10.0-38-generic # optional
apt install sysperf # optional, require GUI
apt install gdb # optional

# Create new "user" to run a QEMU & KVM
# useradd -m -p $(perl -e 'print crypt("user", "blue")') -s /bin/bash user
# usermod -a -G libvirtd user

# Configure Linux to enable profiling for standard user
# echo kernel.perf_event_paranoid=-1 > /etc/sysctl.d/60-profiling.conf
# echo kernel.kptr_restrict=0 >> /etc/sysctl.d/60-profiling.conf

# other (optional)
apt install cpu-checker
apt install openssh-server
kvm-ok # check kvm is available (by cpu-checker)
service ssh start # to connect to machine via ssh

apt autoremove && apt autoclean

# manual qemu installation
git clone git://git.qemu-project.org/qemu.git
cd qemu
git tag -l
git branch temp_v2.9.1 v2.9.1
git checkout temp_v2.9.1
# compile
./configure --target-list=x86_64-softmmu,x86_64-linux-user,aarch64-softmmu,aarch64-linux-user --enable-debug
make -j4
```

Setup Ubuntu server VM
```bash
# wget https://mirror.yandex.ru/ubuntu-releases/18.04/ubuntu-18.04-live-server-amd64.iso
# wget https://mirror.yandex.ru/ubuntu-releases/16.04.4/ubuntu-16.04.4-server-i386.iso
wget https://mirror.yandex.ru/ubuntu-releases/16.04.4/ubuntu-16.04.4-server-amd64.iso

# create 10G image with qcow2 format
sudo qemu-img create -f qcow2 ubuntu-kvm-volume.img 10G
# create vm with kvm, 1Gb RAM, 2 CPU, port forwarding (host)22:7777(guest)
qemu-system-x86_64 -m 1024 -boot d -enable-kvm -smp 2 -net nic -net user,hostfwd=tcp::7777-:22 -hda ubuntu-kvm-volume.img -cdrom ubuntu-16.04.4-server-amd64.iso
# WARNING: next start should be without `-cdrom ubuntu-16.04.4-server-amd64.iso`
```

Run program:
```bash
# executable files will be located in /bin
make
# copy on VM
scp -r /bin -P 7777 <username>@<guest-ip>:<path>
```

Possible `perf` usage:
```bash
# SHOULD BE RUN UNDER ROOT USER
perf record -ag # system-wide profiling
# or
perf -g -p <pid> # for certain process
# wait for some time and stop execution by ^C
perf report
```

Possible `gdb` usage:
```bash
# SHOULD BE RUN UNDER ROOT USER
gdb <process> # start debugging
break <breakpoint-name> # setup breakpoint
run <file>
bt # stop on breakpoint
info registers
disassemble
continue
```

Decompilation:
```bash
objdump -d <filename>
```

### Sources:
* [QEMU port forwarding](https://unix.stackexchange.com/questions/124681/how-to-ssh-from-host-to-guest-using-qemu#124777)
* [GCC debug options](https://gcc.gnu.org/onlinedocs/gcc/Debugging-Options.html)
* [Ubuntu releases](https://mirror.yandex.ru/ubuntu-releases/)
* [How to use QEMU](https://fosspost.org/tutorials/use-qemu-test-operating-systems-distributions)
* [Port forwarding](https://www.cnx-software.com/2011/10/02/how-to-transfer-files-between-host-and-qemu-via-ssh-and-nfs/)
* [Install kvm machines on ubuntu 16.04](https://www.cyberciti.biz/faq/installing-kvm-on-ubuntu-16-04-lts-server)
